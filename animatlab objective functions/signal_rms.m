function objective = signal_rms( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(2,num_trials);

    for i=1:num_trials
        time = sim_data_cell{i}(1:end-10,2);
        m = length(time);
        start_ss = floor(0.167*m);
        end_ss = floor(0.833*m);
        
        time = time(start_ss:end_ss);
%         dt = mean(diff(time));
        
        pos = sim_data_cell{i}(start_ss:end_ss,3);
        amp = (60*pi/180)/4;
        expected_pos = 2*amp + amp*cos(0.5*6.28*time - pi);
        
        [~,locs] = findpeaks(smooth(pos,50));
        
        h = figure;
        subplot(2,1,1)
        hold on
        plot(time,pos,'blue')
        plot(time,expected_pos,'red')
        grid on
        hold off
        
        
        loc1 = locs(1);
        pk1 = pos(loc1);
        loc2 = locs(find(locs > floor(m/3),1,'first'));
        pk2 = pos(loc2);
        loc3 = length(time);
        pk3 = pos(end);
        
        extending = sim_data_cell{i}(start_ss:end_ss,10);
        load = sim_data_cell{i}(start_ss:end_ss,4);
        
        objective(1,i) = rms(1000*(pos - expected_pos));
        
%         objective(2,i) =    (1000*(pk1 - expected_pos(1))).^2 +...
%                             (1000*(pk2 - expected_pos(floor(end/2)))).^2 +...
%                             (1000*(pk3 - expected_pos(end))).^2;
        objective(2,i) =    (1000*(pk1 - expected_pos(1))).^2 +...
                            (1000*(pk2 - expected_pos(floor(end/2)))).^2;
       
        figure(h)
        subplot(2,1,1)
        hold on
        plot(time([loc1,loc2]),[pk1,pk2],'magenta+')
        hold off
        
        subplot(2,1,2)
        hold on
        plot(time,extending,'green')
        plot(time,load,'magenta')
        legend('Extending','Load')
        grid on
        hold off
        
%         keyboard
        
    end

end

