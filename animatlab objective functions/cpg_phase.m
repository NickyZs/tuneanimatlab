function objective = cpg_phase(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(1,num_trials);
    
    smooth_window = 25;
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_time = 8; %s
        start_ss = find(sim_data_cell{i}(:,2) >= start_time, 1, 'first');
%         start_ss = 1;
        
        time = sim_data_cell{i}(start_ss:m,2);
        
        ext_hc = smooth(sim_data_cell{i}(start_ss:m,4),smooth_window);
        input = -sim_data_cell{i}(start_ss:m,9);
        
        ext_phase = signal_to_phase(time,ext_hc);
        in_phase = signal_to_phase(time,input);
        
        pd = in_phase - ext_phase;
%         phase_diff = pd(mod(in_phase,1) == 0);
        phase_diff = pd(mod(in_phase,0.5) == 0);
        
        figure
        subplot(2,3,[3,6])
        stairs(phase_diff,'linewidth',2)
        xlim([1,length(phase_diff)])
        hold on
        grid on
        
        subplot(2,3,4)
        stairs(diff(phase_diff),'linewidth',2)
        hold on
        grid on
        
%         subplot(2,2,1)
%         plot(mod(ext_phase(mod(in_phase,0.5)==0),1),mod(phase_diff,1),'b.')
%         hold on
%         xlim([0,1])
%         grid on
        
%         figure
%         plot(time(mod(in_phase,0.5) == 0),ext_phase(mod(in_phase,0.5) == 0))
%         hold on
%         plot(time(mod(in_phase,0.5) == 0),in_phase(mod(in_phase,0.5) == 0))
%         
%         figure
%         plot(time(1:end-1),diff(ext_phase))
                
        
        objective{i,1} = phase_diff;
    end
        
end
