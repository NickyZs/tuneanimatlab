function objective = nlls_with_penalty( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(num_trials,1);
    desired_time = desired_data(:,1);
    desired_states = desired_data(:,2:end);

    desired_max = 0.5;
    desired_min = 0;
    desired_period = 2.5;
    
%     num_states = size(desired_states,2);
    for i=1:num_trials
        %First column is timeslice; ignore it.
        system_time = sim_data_cell{i}(:,2);
        temp_system_states = sim_data_cell{i}(:,3:end); 
        system_states = NaN(size(desired_states));
        
        %kinematic comparison, one column only
        system_states(:,1) = interp1(system_time,temp_system_states(:,1),desired_time);
        kinematic_penalty = 0.5 * sum(sum( (system_states - desired_states).^2 ));
        
        measured_period = find_period(system_states,2,0,mean(diff(desired_time)));

        period_penalty = 0.5*1e3*(desired_period - measured_period).^2;
        if isnan(period_penalty)
            period_penalty = 1e6;
        end
        
        %amplitude_comparison
        max_pks = findpeaks(system_states(:,i));
        min_pks = findpeaks(-system_states(:,i));
        
        max_pks_diff = 1e3*diff(max_pks);
        if isempty(max_pks_diff)
            max_pks_diff = 1e3;
        end
        
        min_pks_diff = 1e3*diff(min_pks);
        if isempty(min_pks_diff)
            min_pks_diff = 1e3;
        end
       
        max_amp_penalty = 0.5*1e3*sum(((desired_max - max_pks)/length(max_pks)).^2);
        min_amp_penalty = 0.5*1e3*sum(((desired_min - min_pks)/length(min_pks)).^2);
        
        max_pks_diff_penalty = 0.5*sum((max_pks_diff/length(max_pks_diff)).^2);
        min_pks_diff_penalty = 0.5*sum((min_pks_diff/length(min_pks_diff)).^2);
%         max_amp_penalty = 0.5*1e3*(desired_max - max(system_states(:,i))).^2;
%         min_amp_penalty = 0.5*1e3*(desired_min - min(system_states(:,i))).^2;

%         objective(i) = kinematic_penalty + max_amp_penalty + min_amp_penalty + period_penalty;
        objective(i) = max_amp_penalty + min_amp_penalty + period_penalty;
%         objective(i) = max_amp_penalty + min_amp_penalty + period_penalty + max_pks_diff_penalty + min_pks_diff_penalty;

%         figure
%         clf
%         hold on
%         grid on
%         title(['desired (',num2str(length(desired_time)),')'])
%         plot(desired_time,desired_states)
%         hold off
%         
%         figure
%         clf
%         hold on
%         grid on
%         title(['raw sim output (',num2str(length(system_time)),')'])
%         plot(system_time,temp_system_states)
%         hold off
%         
%         figure
%         clf
%         hold on
%         grid on
%         title(['interpolated sim output (',num2str(length(desired_time)),')'])
%         plot(desired_time,system_states)
%         hold off
%         keyboard
    end

end

