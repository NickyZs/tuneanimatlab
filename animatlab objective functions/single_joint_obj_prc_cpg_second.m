function objective = single_joint_obj_prc_cpg_second(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(3,num_trials);
    
    smooth_window = 5; %25; %250;
    downsample_window = 1;
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
      
        ext_hc = downsample(sim_data_cell{i}(start_ss:m,5),downsample_window);
        flx_hc = downsample(sim_data_cell{i}(start_ss:m,6),downsample_window);
        time = downsample(sim_data_cell{i}(start_ss:m,2),downsample_window);
        dt = mean(diff(time));
        
        ext_hc = smooth(ext_hc,smooth_window);
        flx_hc = smooth(flx_hc,smooth_window);
                
        period = find_period(ext_hc,0.25,1,dt);
        
        ext_threshold = min(ext_hc)+0.85*range(ext_hc);
        flx_threshold = min(flx_hc)+0.85*range(flx_hc);
        
%         falling_edge_inds = find([0;diff( ext_hc > ext_threshold) > 0]); %falling edges
%         rising_edge_inds = find([0;diff( flx_hc > flx_threshold) > 0]); %rising edges

        falling_edge_inds = find([0;diff( ext_hc > ext_threshold) < 0]); %falling edges
        rising_edge_inds = find([0;diff( flx_hc > flx_threshold) < 0]); %rising edges

        fall_edge_len = length(falling_edge_inds);
        if fall_edge_len >= 4
            falling_edge_inds = falling_edge_inds(1:4);
            falling_edges = time(falling_edge_inds);
        elseif fall_edge_len < 4
            falling_edges = time(falling_edge_inds);
        end
        
        rise_edge_len = length(rising_edge_inds);
        if rise_edge_len >= 4
            rising_edge_inds = rising_edge_inds(1:4);
            rising_edges = time(rising_edge_inds);
        elseif rise_edge_len < 4
            rising_edges = time(rising_edge_inds);
        end

        objective{1,i} = period;
        objective{2,i} = falling_edges;
        objective{3,i} = rising_edges;
        
%         h = figure;
%         plot(time,ext_hc)
%         hold on
%         plot(time,flx_hc,'g')
%         plot(falling_edges,ext_hc(falling_edge_inds),'bo')
%         plot(rising_edges,flx_hc(rising_edge_inds),'go')
%         hold off
%         drawnow
    end
        
end
