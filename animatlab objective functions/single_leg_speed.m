function objective = single_leg_speed( sim_data_cell, desired_data )
%SINGLE_LEG_SPEED Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = cell(4,num_trials);

    for i=1:num_trials
        m = size(sim_data_cell{i},1) - 10;
        speed = sim_data_cell{i}(1:m,5);
        
        objective(1:2,i) = num2cell([mean(speed);std(speed)]);

        load = sim_data_cell{i}(1:m,6);
        loaded = (load > -59.9e-3);
%         duty = sum(loaded)/m;
        
        time = sim_data_cell{i}(1:m,2);
        stance_times = time([false;diff(loaded) == 1]);
        swing_times = time([false;diff(loaded) == -1]);
        period = mean(diff(stance_times));
        
        num_steps = min(length(stance_times),length(swing_times));
        if num_steps > 0
            time_in_stance = mean(swing_times(1:num_steps) - stance_times(1:num_steps));
        else
            time_in_stance = NaN;
        end
            
        duty = time_in_stance/period;
        
        objective{3,i} = (1-duty)*period;
        objective{4,i} = duty*period;
    end
end

