function objective = single_joint_period_0p4(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(1,num_trials);
    
    smooth_window = 25;
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
        
        time = sim_data_cell{i}(start_ss:m,2);
        dt = mean(diff(time));
        
        ext_hc = smooth(sim_data_cell{i}(start_ss:m,5),smooth_window);
                
        period = find_period(ext_hc,0.25,1,dt);
        
        objective{1,i} = (0.4 - period).^2;
    end
        
end
