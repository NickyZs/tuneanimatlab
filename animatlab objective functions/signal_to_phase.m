function phase = signal_to_phase(time,signal)

    hi_threshold = min(signal)+0.50*range(signal);
    lo_threshold = min(signal)+0.50*range(signal);
    
    %falling edges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    maxima_inds = find([0;diff( signal > hi_threshold) == 1]); %rising edges
%     [~,maxima_inds] = findpeaks(signal);
    %rising edges ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    minima_inds = find([0;diff( signal > lo_threshold) == -1]); %falling edges
%     [~,minima_inds] = findpeaks(-signal);
    if maxima_inds(1) > minima_inds(1) 
        %make sure the first rising edge is before the first falling edge.
        minima_inds(1) = [];
    end

    num_cycles = min(length(minima_inds),length(maxima_inds));

    falling_edge_inds(1:num_cycles) = minima_inds(1:num_cycles);
    falling_edges = time(falling_edge_inds);

    rising_edge_inds(1:num_cycles) = maxima_inds(1:num_cycles);
    rising_edges = time(rising_edge_inds);
    
    phase = NaN(size(time));
    for i=1:num_cycles-1
        inds = rising_edge_inds(i):falling_edge_inds(i);
        tt = [rising_edges(i),falling_edges(i)];
        pp = i - 1 + [0,0.5];
        phase(inds) = interp1(tt,pp,time(inds));

        inds = falling_edge_inds(i):rising_edge_inds(i+1);
        tt = [falling_edges(i),rising_edges(i+1)];
        pp = i - 1 + [0.5,1];
        phase(inds) = interp1(tt,pp,time(inds));
    end
    
end