function objective = single_joint_obj_prc(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(2,num_trials);
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
        
        time = sim_data_cell{i}(start_ss:m,2);
        dt = mean(diff(time));
        
        pos = smooth(sim_data_cell{i}(start_ss:m,25),100);
        flx_mn = sim_data_cell{i}(start_ss:m,10);
                
        period = find_period(pos,0.25,1,dt);
        
%         threshold = min(pos)+0.99*range(pos);
%         thresholded_pos = pos > threshold;
%         
%         diff_data = [0;diff(pos)];
%         diff_diff_data = [0;diff(diff_data)];
%         
%         extrema_inds = find( diff((diff_data)>0));
%         maxima_inds = intersect(intersect(extrema_inds, find(diff_diff_data < 0)),find( thresholded_pos ));
%     
%         falling_edge_inds = NaN(4,1);
%         falling_edge_inds(1:min(4,length(maxima_inds))) = maxima_inds(1:min(4,length(maxima_inds)));

        threshold = min(pos)+0.95*range(pos);
%         thresholded_pos = pos > threshold;
        
        diff_data = [0;diff(pos)];
        diff_diff_data = [0;diff(diff_data)];
        
%         extrema_inds = find( diff((diff_data)>0));
        extrema_inds = find([0;diff( pos > threshold) < 0]);
        
%         maxima_inds = intersect(intersect(extrema_inds, find(diff_diff_data < 0)),find( thresholded_pos ));
        maxima_inds = intersect(extrema_inds, find(diff_diff_data < 1e-6));

        falling_edge_inds = NaN(4,1);
        falling_edge_inds(1:min(4,length(maxima_inds))) = maxima_inds(1:min(4,length(maxima_inds)));

        
%         figure
%         clf
%         subplot(2,1,1)
%         hold on
%         grid on
%         plot(time,flx_mn)
%         hold off
%         subplot(2,1,2)
%         hold on
%         grid on
%         plot(time,pos)
%         plot(time(extrema_inds),pos(extrema_inds),'go')
%         plot(time(maxima_inds),pos(maxima_inds),'r+')
%         hold off

        if any(isnan(falling_edge_inds))
            falling_edges = NaN(4,1);
        else
            falling_edges = time(falling_edge_inds);
        end
        
%         period
%         falling_edges
        
        objective{1,i} = period;
        objective{2,i} = falling_edges;
        
%         figure
%         clf
%         hold on
%         grid on
%         plot(time,pos)
%         plot(time(extrema_inds),pos(extrema_inds),'go')
%         plot(time(maxima_inds),pos(maxima_inds),'r+')
%         hold off
%         
%         figure
%         clf
%         hold on
%         plotyy(time,pos,time,diff_diff_data)
%         hold off



%         keyboard
        
    end
    
end
