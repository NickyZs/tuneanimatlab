function objective = single_joint_obj_prc_cpg(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(1,num_trials);
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
        
        time = sim_data_cell{i}(start_ss:m,2);
        dt = mean(diff(time));
        
        pos = smooth(sim_data_cell{i}(start_ss:m,25),100);
        
        objective{i} = pos(end);

%         figure
%         clf
%         subplot(2,1,1)
%         hold on
%         grid on
%         plot(time,pos)
%         hold off
%         subplot(2,1,2)
%         hold on
%         grid on
%         plot(time,ext_hc)
%         plot(time(extrema_inds),ext_hc(extrema_inds),'go')
%         plot(time(maxima_inds),ext_hc(maxima_inds),'r+')
%         hold off
% 
%         keyboard
        
    end
    
end
