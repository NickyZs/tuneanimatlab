function objective = calculate_k( sim_data_cell, desired_data )
%SINGLE_LEG_SPEED Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = cell(2,num_trials);

    for i=1:num_trials
        m = size(sim_data_cell{i},1) - 10;
                
        time = sim_data_cell{i}(1:m,2);
        load = sim_data_cell{i}(1:m,11);
        des_load = sim_data_cell{i}(1:m,10);
        
        loaded = (load > -59.0e-3);
        load_diff = diff(loaded);
        r_edge = find([false;(load_diff == 1)])+30;
        f_edge = find([false;(load_diff == -1)])-30;
        
        step_dur = time(f_edge) - time(r_edge);
        step_to_remove = (step_dur < 0.25);
        step_dur(step_to_remove) = [];
        r_edge(step_to_remove) = [];
        f_edge(step_to_remove) = [];
        
        num_steps = length(step_dur);
        force = NaN(num_steps,1);
        des_force = NaN(num_steps,1);
        
        for j=1:num_steps
            force(j) = mean(load(r_edge(j):f_edge(j))) + 0.060;
            des_force(j) = mean(des_load(r_edge(j):f_edge(j))) + 0.060;
        end
        
        kdt = NaN(num_steps-1,1);
        
        for j=1:num_steps-1
            if abs((force(j)-des_force(j))/des_force(j)) > 0.100
                %If the recordings are sufficiently different, calculate k.
                kdt(j) = (force(j+1)-force(j))/(des_force(j)-force(j));
            end
        end
        
        figure
        subplot(3,1,1)
        plot(time,load)
        hold on
        grid on
        plot(time(r_edge),load(r_edge),'go')
        plot(time(f_edge),load(f_edge),'r+')
        plot(time(floor((r_edge+f_edge)/2)),force-.060,'b*')
        
        subplot(3,1,2)
        stairs([force;0])
        hold on
        title('force magnitudes')
        grid on
        hold off
        
        subplot(3,1,3)
        plot(kdt)
        hold on
        title('k\cdot\Deltat')
        grid on
        hold off
        
        objective{1,i} = mean(kdt(~isnan(kdt)));
        objective{2,i} = kdt;
    end
end

