function period = cpg_period(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    if num_trials > 1
        error('Too many trials.')
    end
    
    smooth_window = 25;
    
    m = size(sim_data_cell{1},1)-10;
    start_ss = floor(0.1*m);

    time = sim_data_cell{1}(start_ss:m,2);
    dt = mean(diff(time));

    ext_hc = smooth(sim_data_cell{1}(start_ss:m,4),smooth_window);

    period = find_period(ext_hc,0.25,1,dt);
        
end
