function objective = jerk( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(2,num_trials);
%     objective = NaN(2,num_trials);
    
%     num_states = size(desired_states,2);
    for i=1:num_trials
        %First column is timeslice; ignore it.
        time = sim_data_cell{i}(:,2);
        position = sim_data_cell{i}(:,5);
        expected_position = -pi/6+pi/6*cos(2*pi/10*time);
        objective(1,i) = rms(position - expected_position);
        
        %first, remove redundant points
        new_inds = find(position(1:end-1) ~= position(2:end));
        
        new_position = position(new_inds);
        new_time = time(new_inds);
        
        vel_time = 0.5*(new_time(1:end-1) + new_time(2:end));
        vel = diff(new_position)./diff(new_time);
        
        acc_time = 0.5*(vel_time(1:end-1) + vel_time(2:end));
        acc = diff(vel)./diff(vel_time);
        
        jerk_time = 0.5*(acc_time(1:end-1) + acc_time(2:end));
        jerk = diff(acc)./diff(acc_time);
%         expected_jerk = @(t) -pi/6*(2*pi/10)^3*sin(2*pi/10*t);
        
%         jerk_pos = interp1(new_time,new_position,jerk_time);
        
%         figure
%         clf
%         hold on
%         grid on
%         plotyy(new_time,new_position,jerk_time,jerk)
        
        objective(2,i) = rms(jerk);
        
    end

end

