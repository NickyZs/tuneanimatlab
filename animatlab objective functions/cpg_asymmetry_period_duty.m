function objective = cpg_asymmetry_period_duty(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(2,num_trials);
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
        
        time = sim_data_cell{i}(start_ss:m,2);
        dt = mean(diff(time));
        
        hc1 = sim_data_cell{i}(start_ss:m,5);
                
        period = find_period(hc1,0.25,1,dt);
        
        threshold_hi = min(hc1)+0.99*range(hc1);
        threshold_lo = min(hc1)+0.01*range(hc1);
        
        diff_data = [0;diff(hc1)];
        
        maxima_inds = find([0;diff( hc1 > threshold_hi) < 0]); %falling edges
        minima_inds = find([0;diff( hc1 < threshold_lo) > 0]);
        
%         figure
%         plot(time,hc1)
%         hold on
%         plot(time(maxima_inds),hc1(maxima_inds),'ro')
%         plot(time(minima_inds),hc1(minima_inds),'g+')
%         grid on
%         hold off
%         drawnow
        
        if ~isempty(maxima_inds) && ~isempty(minima_inds) &&...
                maxima_inds(1) > minima_inds(1)
            minima_inds(1) = [];
        end
        inds = 1:min(length(maxima_inds),length(minima_inds));
        
        duty = mean(minima_inds(inds)-maxima_inds(inds))/mean(diff(maxima_inds));
        
        objective{1,i} = period;
        objective{2,i} = duty;
    end
        
end
