function objective = jerk_no_downsample( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(2,num_trials);
%     objective = NaN(2,num_trials);
    
%     num_states = size(desired_states,2);
    for i=1:num_trials
        %First column is timeslice; ignore it.
        time = sim_data_cell{i}(:,2);
        position = sim_data_cell{i}(:,5);
        expected_position = -pi/6+pi/6*cos(2*pi/10*time);
        objective(1,i) = rms(position - expected_position);
        
        vel_time = 0.5*(time(1:end-1) + time(2:end));
        vel = diff(position)./diff(time);
        
        %We need a smooth velocity for penalization
        %first, remove redundant points
        new_inds = find(position(1:end-1) ~= position(2:end));
        new_position = position(new_inds);
        new_time = time(new_inds);
        new_vel_time = 0.5*(new_time(1:end-1) + new_time(2:end));
        new_vel = diff(new_position)./diff(new_time);
        expected_velocity = -pi/6*2*pi/10*sin(2*pi/10*new_vel_time);
        
        sum(new_vel.*expected_velocity < 0)
        
        acc_time = 0.5*(vel_time(1:end-1) + vel_time(2:end));
        acc = diff(vel)./diff(vel_time);
        
        jerk_time = 0.5*(acc_time(1:end-1) + acc_time(2:end));
        jerk = diff(acc)./diff(acc_time);
        
        
                
        objective(2,i) = rms(jerk);
        
%         expected_jerk = @(t) -pi/6*(2*pi/10)^3*sin(2*pi/10*t);
        
%         jerk_pos = interp1(new_time,new_position,jerk_time);
        
        figure
        clf
        hold on
        grid on
        title(['pos rms: ',num2str(objective(1,i)),' jerk rms: ',num2str(objective(2,i))])
%         plot(new_time,new_position)
%         plot(jerk_time(norm_jerk > 1e6),jerk_pos(norm_jerk > 1e6),'go')
        
%         plot(vel_time,vel,'g')
        plotyy(time,position,jerk_time,jerk)
        
        figure
        clf
        hold on
        grid on
        plot(new_vel_time,expected_velocity)
        plot(new_vel_time,new_vel,'r')
        hold off
        
        vel_error = new_vel-expected_velocity;
        
        figure
        clf
        hold on
        title(['vel error rms: ',num2str(rms(vel_error))])
        grid on
%         plot(new_time,new_position)
%         plot(jerk_time(norm_jerk > 1e6),jerk_pos(norm_jerk > 1e6),'go')
        
%         plot(vel_time,vel,'g')
        
        plot(new_vel_time,vel_error)
        
        
        
%         figure
%         clf
%         hold on
%         grid on
%         plot(jerk_time,jerk)
%         plot(jerk_time,expected_jerk(jerk_time),'g')
                
%         
%         figure
%         clf
%         hold on
%         grid on
%         title('norm jerk')
%         semilogy(jerk_time,norm_jerk)
%         hold off
%         
%         keyboard
        
        
        
    end

end

