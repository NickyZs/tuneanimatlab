function objective = single_joint_obj(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = NaN(num_trials, 4); %frequency,amplitude,extension_percentage,mean_position,end_position
    
%     des_freq = desired_data(1);
%     des_amp = desired_data(2);
%     des_duty = desired_data(3);
%     des_mean = desired_data(4);
    
    
    for i=1:num_trials
        time = sim_data_cell{i}(1:end-10,2);
        m = length(time);
        start_ss = floor(0.2*m);
        
        time = time(start_ss:m);
        dt = mean(diff(time));
        
        ext_hc = sim_data_cell{i}(start_ss:m,5);
        flx_hc = sim_data_cell{i}(start_ss:m,6);
        pos = sim_data_cell{i}(start_ss:m,25);
        
        len = length(pos);
        
        period = find_period(pos,0.25,1,dt);
        
        if isnan(period) || period <= .25
            frequency = 0;
        else
            frequency = 1/period;
        end
        
        amplitude = range(pos);
        
        if frequency ~= 0
            period_window = ceil(period/dt);
            num_cycles = floor(len/period_window);
            extension_cycles = NaN(num_cycles,1);
                        
            for j=1:num_cycles
                start_ind = 1+(j-1)*period_window;
                end_ind = j*period_window;
                extension_cycles(j) =...
                    sum(ext_hc(start_ind:end_ind) > flx_hc(start_ind:end_ind))/period_window;
            end
            extension_percentage = mean(extension_cycles);
        else
            extension_percentage = sum(ext_hc > flx_hc)/(0.8*m);
        end
        
        mean_position = mean(pos);
        
        end_position = pos(end);
        
%         objective(i,:) = [frequency,amplitude,extension_percentage,mean_position,end_position];
        objective(i,:) = [frequency,amplitude,extension_percentage,mean_position];
    end
    
end
