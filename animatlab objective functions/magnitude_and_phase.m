function objective = magnitude_and_phase(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(2,num_trials);
    
    for i=1:num_trials
%         m = size(sim_data_cell{i},1)-10;
%         start_ss = floor(0.1*m);
%         
%         time = sim_data_cell{i}(start_ss:m,2);
%         dt = mean(diff(time));
%         
%         ext_hc = smooth(sim_data_cell{i}(start_ss:m,5),smooth_window);
%                 
%         period = find_period(ext_hc,0.25,1,dt);
%         
%         threshold = min(ext_hc)+0.90*range(ext_hc);
%         
%         diff_data = [0;diff(ext_hc)];
%         diff_diff_data = [0;diff(diff_data)];
%         
%         extrema_inds = find([0;diff( ext_hc > threshold) < 0]); %falling edges
%         
%         maxima_inds = intersect(extrema_inds, find(diff_diff_data < 1e-6));
% 
%         falling_edge_inds(1:min(4,length(maxima_inds))) = maxima_inds(1:min(4,length(maxima_inds)));
% 
%         falling_edges = time(falling_edge_inds);
%         if length(falling_edges) < 4
%             falling_edges(end+1:4) = NaN;
%         end

        magnitude = -1;
        phase = -1;

        objective{1,i} = magnitude;
        objective{2,i} = phase;
    end
        
end
