function objective = cpg_prc(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(2,num_trials);
    
    smooth_window = 25;
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_ss = floor(0.1*m);
        
        time = sim_data_cell{i}(start_ss:m,2);
        dt = mean(diff(time));
        
        ext_hc = smooth(sim_data_cell{i}(start_ss:m,4),smooth_window);
        speed = smooth(sim_data_cell{i}(start_ss:m,5));
        V_speed = mode(speed);
        
        period = find_period(ext_hc,0.25,1,dt);
        
        threshold = min(ext_hc)+0.50*range(ext_hc); %or 0.90? Nick 16 Nov 16
        
        diff_data = [0;diff(ext_hc)];
        diff_diff_data = [0;diff(diff_data)];
        
%         extrema_inds = find([0;diff( ext_hc > threshold) < 0]); %falling edges
%         
%         maxima_inds = intersect(extrema_inds, find(diff_diff_data < 1e-6));
% 
%         falling_edge_inds(1:min(4,length(maxima_inds))) = maxima_inds(1:min(4,length(maxima_inds)));
% 
%         falling_edges = time(falling_edge_inds);
%         if length(falling_edges) < 4
%             falling_edges(end+1:4) = NaN;
%         end
% 
%         objective{1,i} = period;
%         objective{2,i} = falling_edges;
        
        extrema_inds = find([0;diff( ext_hc > threshold) > 0]); %rising edges
        
        maxima_inds = intersect(extrema_inds, find(diff_diff_data < 1e-6));

        rising_edge_inds(1:min(4,length(maxima_inds))) = maxima_inds(1:min(4,length(maxima_inds)));

        rising_edges = time(rising_edge_inds);
        if length(rising_edges) < 4
            rising_edges(end+1:4) = NaN;
        end

        objective{1,i} = period;
        objective{2,i} = rising_edges;
        objective{3,i} = V_speed;
        
%         figure
%         plot(time,ext_hc)
%         hold on
%         plot(time(rising_edge_inds),ext_hc(rising_edge_inds),'go')
%         grid on
%         title(['Trial ',num2str(i)])
%         hold off
%         drawnow
    end
        
end
