function objective = single_leg_prc( sim_data_cell, desired_data )
%SINGLE_LEG_SPEED Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = cell(4,num_trials);

    for i=1:num_trials
        m = size(sim_data_cell{i},1) - 10;
        
        thresh = -58.9e-3;
                
        load = sim_data_cell{i}(1:m,6);
        loaded = (load > thresh);
        enterstance = sim_data_cell{i}(1:m,8);
        enteringstance = (enterstance > thresh);
        
        time = sim_data_cell{i}(1:m,2);
        dt = mean(diff(time));
        period = find_period(enteringstance,0.25,1,dt);
        
        stance_times = time([false;diff(loaded) == 1]);
        swing_times = time([false;diff(loaded) == -1]);
        
        num_steps = min(length(stance_times),length(swing_times));
        if num_steps > 0
            if swing_times(1) < stance_times(1)
                swing_times(1) = [];
                num_steps = num_steps - 1;
            end
            
            stance_dur = swing_times(1:num_steps) - stance_times(1:num_steps);

            stance_times(stance_dur < 0.1) = [];
            stance_dur(stance_dur < 0.1) = [];
            
            time_in_stance = mean(stance_dur);
        else
            time_in_stance = NaN;
        end
            
        duty = time_in_stance/period;
        
        
        rising_edges = stance_times;
        if length(stance_times) < 4
            rising_edges(end+1:4) = NaN;
        end
            
        objective{1,i} = period;
        objective{2,i} = rising_edges;
        objective{3,i} = (1-duty)*period;
        objective{4,i} = duty*period;
        
%         figure
%         plot(time,enterstance)
%         hold on
%         plot(time,load,'r')
%         legend('enter swing','load')
%         grid on
%         plot(stance_times,-.060+zeros(size(stance_times)),'b*')
%         hold off
    end
end

