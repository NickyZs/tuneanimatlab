function objective = differentiator_gain(sim_data_cell, desired_data)

    num_trials = length(sim_data_cell);
    objective = cell(num_trials,1);
    
    for i=1:num_trials
        m = size(sim_data_cell{i},1)-10;
        start_time = 0; %s
        start_ss = find(sim_data_cell{i}(:,2) >= start_time, 1, 'first');
%         start_ss = 1;
        time = sim_data_cell{i}(start_ss:m,2);
        v_in = sim_data_cell{i}(start_ss:m,3);
        v_out = sim_data_cell{i}(start_ss:m,6);
        
        mean_in_rate = mean( diff(v_in)./diff(time));
%         mean_out_rate = mean(v_out( (v_in > -0.060) & (v_out < 0) ) ) + .060; 
        mean_out_rate = v_out(end)+0.060;
        
        objective{i} = mean_out_rate/mean_in_rate;
    end
        
end
