function objective = RM_phase( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(num_trials,1);

    desired_max_CTr = 0.3;
    desired_min_CTr = -.3;
    desired_max_FTi = .34;
    desired_min_FTi = -.17;
    desired_period = 2;
    
%     num_states = size(desired_states,2);
    for i=1:num_trials
        %First column is timeslice; ignore it.
        system_time = sim_data_cell{i}(:,2);
        temp_system_states = sim_data_cell{i}(:,3:end); 
        
        %kinematic comparison, one column only
        CTr = temp_system_states(:,1);
        FTi = temp_system_states(:,2);
%         CTr = interp1(system_time,temp_system_states(:,1),desired_time);
%         FTi = interp1(system_time,temp_system_states(:,2),desired_time);
        
%         measured_period = find_period(CTr,2,0,mean(diff(system_time)));
        measured_period = find_period(CTr,2,1,mean(diff(system_time)));

        period_penalty = 0.5*1e3*(desired_period - measured_period).^2
        if isnan(period_penalty)
            period_penalty = 1e6;
        end
        
        %amplitude_comparison
        max_pks_CTr = findpeaks(CTr);
        min_pks_CTr = findpeaks(-CTr);
       
        max_amp_penalty_CTr = 0.5*1e3*sum(((desired_max_CTr - max_pks_CTr)/length(max_pks_CTr)).^2)
        min_amp_penalty_CTr = 0.5*1e3*sum(((desired_min_CTr - min_pks_CTr)/length(min_pks_CTr)).^2)
        
        %amplitude_comparison
        max_pks_FTi = findpeaks(FTi);
        min_pks_FTi = findpeaks(-FTi);
       
        max_amp_penalty_FTi = 0.5*1e3*sum(((desired_max_FTi - max_pks_FTi)/length(max_pks_FTi)).^2)
        min_amp_penalty_FTi = 0.5*1e3*sum(((desired_min_FTi - min_pks_FTi)/length(min_pks_FTi)).^2)

        objective(i) = period_penalty + max_amp_penalty_CTr + min_amp_penalty_CTr + ...
            max_amp_penalty_FTi + min_amp_penalty_FTi;

%         figure
%         clf
%         hold on
%         grid on
%         title(['desired (',num2str(length(desired_time)),')'])
%         plot(desired_time,desired_states)
%         hold off
%         
%         figure
%         clf
%         hold on
%         grid on
%         title(['raw sim output (',num2str(length(system_time)),')'])
%         plot(system_time,temp_system_states)
%         legend('CTr','FTi');
%         hold off
%         
%         keyboard
    end

end

