function objective = nlls( sim_data_cell, desired_data )
%NLLS Summary of this function goes here
%   Detailed explanation goes here
    num_trials = length(sim_data_cell);
    objective = NaN(num_trials,1);
    desired_time = desired_data(:,1);
    desired_states = desired_data(:,2:end);
    
    num_states = size(desired_states,2);
    for i=1:num_trials
        %First step is timeslice.
        system_time = sim_data_cell{i}(:,2);
        temp_system_states = sim_data_cell{i}(:,3:end); 
        system_states = NaN(size(desired_states));
        
        for j=1:num_states
            system_states(:,j) = interp1(system_time,temp_system_states(:,j),desired_time);
        end
        objective(i) = 0.5 * sum(sum( (system_states - desired_states).^2 ));
        
%         figure(1)
%         clf
%         hold on
%         grid on
%         title(['desired (',num2str(length(desired_time)),')'])
%         plot(desired_time,desired_states)
%         hold off
%         
%         figure(2)
%         clf
%         hold on
%         grid on
%         title(['raw sim output (',num2str(length(system_time)),')'])
%         plot(system_time,temp_system_states)
%         hold off
%         
%         figure(3)
%         clf
%         hold on
%         grid on
%         title(['interpolated sim output (',num2str(length(desired_time)),')'])
%         plot(desired_time,system_states)
%         hold off
    end

end

